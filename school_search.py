"""
Utility to search school from the given data set of Schools

@Author: Krunal Jain <krunal10@gmail.com>
"""

import csv
import time


def pre_process() -> dict:
    """
    Reads the CSV file and processes it,
        in such a way that searching school could be done efficiently.

    Note: Pre-processing does take significant time,
        as we are creating unique keywords map,
        so that searching is very fast later on this map.

    :return: keyword_map
        {
            "<word>": [
                ("<state>", "<city>", "<school>"),
                ("<state>", "<city>", "<school>"),
                ...
            ]
        }
    """
    words_map = {}

    # Read CSV File
    rows = csv.DictReader(open("school_data.csv"))

    # Generate keywords map
    for row in rows:
        # Create tuple to add in keyword_map
        school_tuple = (row["LSTATE05"], row["LCITY05"], row["SCHNAM05"])

        # Get all unique words without special characters from the school tuple
        words = set(
            # State only has single word without any special char
            [school_tuple[0]]
            # City name may have space but no other special character
            + school_tuple[1].split(' ')
            # Removing word "SCHOOL" and all other special characters,
            #   from each word in the school name
            + [''.join(character for character in word if character.isalnum())
               for word in school_tuple[2].replace("SCHOOL", "").split(' ')]
        )

        for word in words:
            # Adding school tuple in keyword map
            if word in words_map:
                # Adding school tuple only if it's not already present
                if school_tuple not in words_map[word]:
                    words_map[word].append(school_tuple)
            # Adding new word and thus, school tuple in keyword map
            else:
                words_map[word] = [school_tuple]
    return words_map


# Call to pre-process data
keywords_map = pre_process()


def sanitize_query(query):
    """
    Sanitize query and return keywords
    :param query: query string
    :return: list of keywords
    """
    query = query.upper()
    # Strip "SCHOOL" because it generates tons of false matches
    query.replace("SCHOOL", "")
    return query.split()


def get_top_three_matches(keywords):
    """
    Gets top 3 rows matched
    :param keywords: list of keywords to be matched
    :return: List of top 3 rows matched most
    """
    # All unique matched rows
    matched_rows = set([
        matched_tuple for keyword in keywords
        for matched_tuple in keywords_map.get(keyword) or []
    ])

    weighted_tuples = []
    for matched_row in matched_rows:
        weight = 0
        for keyword in keywords:
            # Match keyword with school name
            # Strip "SCHOOL" because it generates tons of false matches
            if keyword in matched_row[2].replace("SCHOOL", ""):
                # Matching school names weighted most heavily than others
                weight += 3
                continue

            # Match keyword with city name
            if keyword in matched_row[1]:
                # Matching city weighted higher than state
                weight += 2
                continue

            # Match keyword with state
            if keyword in matched_row[0]:
                weight += 1
                continue

            # if keyword is number give higher priority
            try:
                int(keyword)
                weight += 1
            except:
                pass

        # Append the weight and matched tuple
        weighted_tuples.append((weight, matched_row))

    # Getting top 3 weighted rows
    count, top_matched_rows = 1, []
    for weighted_tuple in sorted(
            weighted_tuples, key=lambda x: x[0], reverse=True
    ):
        if count > 3:
            break

        top_matched_rows.append(weighted_tuple[1])
        count += 1
    return top_matched_rows


def search_schools(query):
    """
    Searches best three results from given data set
    :param query: search string
    :return: top 3 schools in list format
    """
    keywords = sanitize_query(query)

    # Generate matching entries, wrap to determine query time
    start_time = time.time()
    matched_rows = get_top_three_matches(keywords)
    total_time = round(time.time() - start_time, 3)
    print(f'\nResults for "{query}" (search took: {total_time}s)')

    count = 1
    for matched_row in matched_rows:
        print(f'{count}. {matched_row[2]}')
        print(f'{matched_row[1]}, {matched_row[0]}')
        count += 1


if __name__ == '__main__':
    search_schools("elementary school highland park")
    search_schools("jefferson belleville")
    search_schools("riverside school 44")
    search_schools("granada charter school")
    search_schools("foley high alabama")
    search_schools("KUSKOKWIM")
