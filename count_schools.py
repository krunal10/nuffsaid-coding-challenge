"""
Utility to print few metrics on given data set of Schools

@Author: Krunal Jain <krunal10@gmail.com>
"""

import csv
import operator
import time


def increment_count(dict_data: dict, key):
    """
    Increments count value by 1 in the given dict
    :param dict_data: dict data
    :param key: key to increment count
    :return: None
    """
    if key in dict_data:
        dict_data[key] += 1
    else:
        dict_data[key] = 1


def pre_process():
    """
    Reads the CSV file and processes it,
        in such a way that counting could be done efficiently
    :return:
        total_schools, schools_by_states, schools_by_locale, schools_by_cities
    """
    total_schools = 0  # Count of total schools
    schools_by_states = {}  # Count of schools by states
    schools_by_locale = {}  # Count of schools by locale
    schools_by_cities = {}  # Count of schools by cities

    # Read CSV File
    rows = csv.DictReader(open("school_data.csv"))

    for row in rows:
        # counting total number of schools in given data set
        total_schools += 1

        # counting total number of schools in each state
        increment_count(schools_by_states, row["LSTATE05"])

        # counting total number of schools in each locale
        increment_count(schools_by_locale, row["MLOCALE"])

        # counting total number of schools in each city
        increment_count(schools_by_cities, row["LCITY05"])

    return (
        total_schools, schools_by_states, schools_by_locale, schools_by_cities
    )


# Call to pre-process data
total_schools, schools_by_states, schools_by_locale, schools_by_cities = \
    pre_process()


def print_counts():
    """
    Read, Process data set and thus, print all the given count metrics
    :return: None
    """
    start_time = time.time()

    # Printing total number of schools
    print(f"Total Schools: {total_schools}")

    # Printing total number of schools in each state,
    #   sorted by number of schools (in descending order)
    print("Schools by State:")
    for state in sorted(
            schools_by_states.items(), key=operator.itemgetter(1), reverse=True
    ):
        print(f"{state[0]}: {state[1]}")

    # Printing total number of schools in each locale,
    #   sorted by locales (in ascending order)
    print("Schools by Metro-centric locale:")
    for locale in sorted(schools_by_locale.items()):
        print(f"{locale[0]}: {locale[1]}")

    # Printing the city with most number of schools
    max_city, max_count = max(
        schools_by_cities.items(), key=operator.itemgetter(1)
    )
    print(f"City with most schools: {max_city} ({max_count} schools)")

    # Printing cities with at least 1 school
    print(f"Unique cities with at least one school: {len(schools_by_cities)}")

    # Printing total time taken
    print(f"Total time: {round(time.time() - start_time, 5)} seconds")


if __name__ == "__main__":
    print_counts()
